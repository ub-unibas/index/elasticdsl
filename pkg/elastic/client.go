package elastic

import "go.ub.unibas.ch/middleware/v2/pkg/dsl"

type Client interface {
	Info() (*ResultInfo, error)
	GetDSLAPI() *dsl.API
	Search(index string, srch any) (*SearchResult, error)
}
