package middleware

import "go.ub.unibas.ch/middleware/v2/pkg/dsl"

type Aggregation interface {
	GetName() string
	GetAgg(api *dsl.API) dsl.BaseAgg
	UnmarshalJSON([]byte) error
}
